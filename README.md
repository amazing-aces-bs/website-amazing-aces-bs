Dies ist der Quellcode zur Website der Amazing Aces Braunschweig - Stammtisch für Menschen im asexuellen Spektrum.

Die Website ist aktuell unter [amazing-aces-bs.de](http://amazing-aces-bs.de) erreichbar.

Damit zukünftige Organisator\*innen möglichst einfach die Administration dieser Website übernehmen können, ist hier die Vorgehensweise dokumentiert, um die Website zu aktualisieren und zu veröffentlichen.

## Systemvoraussetzungen

Um thumbnail-Bilder zu generieren, wird [ImageMagick](https://imagemagick.org/) verwendet. ImageMagick muss also installiert sein, um die Website-Generierung lokal testen zu können (und natürlich auch auf dem Server).

Die Website wird mit [nanoc](https://nanoc.ws/) generiert, welches in ruby programmiert ist. Wer die Seite bearbeiten möchte, muss also **ruby und nanoc installiert** haben, sowie einige weitere Gems. Nachdem ruby installiert ist, geht der Rest am einfachsten durch einen Aufruf von `bundle install`.

Um die Website manuell zu veröffentlichen, muss außerdem ein **Zugang auf den Server** (z.B. per SSH-Key) vorhanden sein, auf dem die Website liegt. Für einen neuen Server muss ggf. der Pfad in der Datei *nanoc.yaml* angepasst werden. Zugang und genauere Infos zum aktuellen Server und der Domain bekommt ihr ggf. vom aktuellen Orga-Team.

**Update:** inzwischen wird die Seite einmal pro Tag automatisch neu kompiliert und veröffentlicht. Es wird immer die aktuellste Version aus diesem git-Repository verwendet. Um Änderungen zu veröffentlichen, braucht ihr also vorwiegend nur noch Schreibzugriff auf dieses git-Repository. Den SSH-Zugang zum Server sollte ein Admin natürlich trotzdem haben - z.B. falls mal etwas sofort aktualisiert werden soll, und nicht erst mit dem automatischen Update am nächsten Tag.

## Vorgehensweise zum Veröffentlichen von Änderungen

Die Änderungen müssen zuerst lokal gespeichert und getestet werden. Wenn die entsprechenden Dateien (siehe folgende Abschnitte) geändert und gespeichert sind, öffnet dazu im Hauptorder der Seite (dort, wo auch das .git-repo liegt) ein Terminal und führt den Befehl `bundle exec nanoc` aus.

In einem weiteren Terminal kann mit `bundle exec nanoc view` eine Vorschau gestartet werden, die dann im Webbrowser unter *localhost:3000* sichtbar ist, so lange bis der Vorschau-Prozess beendet wird.  
Diese Vorschau wird auch aktualisiert, wenn Dateien verändert wurden und ihr erneut `bundle exec nanoc` ausführt - auch ohne die Vorschau zu beenden und neu zu starten.

Wenn ihr mit allen Änderungen zufrieden seid, dann könnt ihr die Änderungen in git committen und entweder mit `git push` direkt auf Codeberg hochladen (Zugriffsrechte dafür ggf. beim Orga-Team erfragen), oder einen Pull-Request erstellen. Einmal pro Tag wird die Seite automatisch (mit dem aktuellen main-branch von Codeberg) auf dem Server neu kompiliert und veröffentlicht.

## Website-Struktur

Das Layout der Seite ist im Format [slim](http://slim-lang.com/) definiert - vor allem in den Dateien *layouts/default.slim* und *content/index.slim*.

Der eigentliche Inhalt ist, bis auf die Startseite und die Blog- und Termin-Übersichtsseiten, in [Markdown](https://markdown.de/) geschrieben. Für jede weitere Seite (also z.B. Blogposts, Info-Seiten wie z.B. FAQ, und jeweils eine Ankündigungsseite für jede Stammtisch-Veranstaltung) liegt ein eigenes Unter-Verzeichnis im Ordner *content* mit einer eigenen *index.md*-Datei und ggf. weiteren Dateien, die nur für diese Seite nötig sind, also z.B. Bildern. Der Name des Ordners taucht dann in der fertigen Website als Teil der URL auf, also z.B. *https://amazing-aces-bs.de/kontakt/* für den Ordner *content/kontakt/*.

Auf der Startseite werden automatisch die drei nächsten Termine (vom Zeitpunkt der letzten Aktualisierung aus gesehen) und die drei neusten Blogposts jeweils in einer kleinen Vorschau-Box angezeigt.

Für Termine wird (neu seit Herbst 2023) außerdem ein iCal-Kalender generiert und Links zum Abonnieren des Kalenders an mehreren Stellen eingefügt.

#### Neu seit Dezember 2024: Archivierte Beiträge

Um den Ordner *content* übersichtlich zu halten, können ältere Beiträge (vor allem die Seiten zu vergangenen Terminen) archiviert werden. Dazu kann der Ordner des jeweiligen Termin in den Ordner *content/archive* verschoben werden. Die Seite wird dann immer noch genauso generiert, und die Adresse ändert sich nicht.

### Neuen Stammtischtermin und/oder Blogpost anlegen

Um einen neuen Termin anzulegen, ist es am einfachsten, zuerst den Ordner eines vergangenen Termins zu kopieren und den Namen entsprechend anzupassen. Dann im angepassten Ordner die Datei *index.md* bearbeiten, so dass die Daten für den neuen Termin passend sind. Aktuell können bei Termin-Seiten und Blogposts folgende Infos im Header-Bereich angegeben werden:

Attribut-Name | Beispiel-Wert | Beschreibung
------------- | ------------- | ------------
title | "Amazing Aces Stammtisch im Oktober 2023" | Titel für die Seite
subtitle | Unser monatliches Treffen für alle Menschen im asexuellen Spektrum | Untertitel für die Seite
tags | Veranstaltung, Stammtisch | Themen-Stichwörter, werden für Blogposts in der Vorschaubox angezeigt
published | 2023-09-12 | Datum und Uhrzeit (default 0 Uhr), wann diese Seite erstellt/veröffentlicht wurde
updated | 2023-09-26T17:00 | Datum und Uhrzeit, wann diese Seite zuletzt geändert wurde
news | false | gibt an, ob es sich (auch) um einen Blogpost handelt (*true* für ja)
nometa | true | gibt an, ob die Meta-Informationen (*published* und *tags*) unter der Titelzeile ausgeblendet werden sollen
eventdate | 2023-10-12T20:00 | (Nur für Termine) Datum und Start-Uhrzeit für Terminvorschau-Box und Kalender-Export
eventend | 2023-10-12T22:00 | (Nur für Termine) Datum und Uhrzeit, wann der Termin endet
eventname | "Ace-Stammtisch Oktober 2020" | (Nur für Termine) der Name der Veranstaltung für die Terminvorschau-Box
eventtype | "Online-Stammtisch" | (Nur für Termine) Zusätzliche Zeile für die Art des Termins
eventlocation | "BigBlueButton" | (Nur für Termine) Name des Orts, wo der Termin stattfindet
~~eventdetail~~ | ~~"ab 19:00 Uhr"~~ | ~~(Nur für Termine) Weitere Infos für die Terminvorschau-Box, z.B. Uhrzeit und/oder Ort~~ (veraltet)
thumbnail | flyer.jpg | (Nur für Blogposts) Pfad zu einem Bild, das in der Vorschaubox angezeigt werden soll

Eine Seite kann auch gleichzeitig Blogpost (*news: true*) und Terminankündigung (*eventdate* etc. sind angegeben) sein, dann taucht sie als Vorschaubox im jeweiligen Format sowohl auf der Übersicht der Termine (so lange der Termin in der Gegenwart oder Zukunft liegt) als auch auf der Blog-Übersicht (dauerhaft) auf. Das ist vor allem für besondere Veranstaltungen wie z.B. Lesungen, Infostände o.ä. interessant, auf die ihr zusätzlich im Blog hinweisen wollt.

Unterhalb des Header-Bereichs in der jeweiligen *index.md*-Datei folgt dann der Beschreibungstext mit allen weiteren Infos, und es können Bilder eingebunden werden, etc.

Falls es sich um eine Veranstaltung handelt, werden automatisch drei Kalender-Links (neu ab Herbst 2023) und ein Absatz mit einem Link zur Kontakt-Seite eingebunden, der in der Layout-Datei definiert ist.