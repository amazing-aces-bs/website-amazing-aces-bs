class Thumbnailize < Nanoc::Filter
    identifier :thumbnailize
    type       :binary

    def run(filename, params={})
        system(
            # use imagemagick for converting an image:
            'magick', # comment out this line for compiling on windows, and uncomment the next one instead
            # 'C:\Program Files\ImageMagick-7.1.1-Q16-HDRI\magick.exe', # path to the magick binary for compiling on windows
            # the [0] is used to extract the first frame from animated gifs:
            filename+"[0]",
            # set background color to purple:
            '-background', '#940b92', 
            # replace any transparent parts with background color:
            '-flatten',
            # set interlacing scheme (for processing raw image formats):
            '-interlace', 'plane', 
            # set image quality (for compression) to 60%:
            '-quality', '60',
            # resize image to the given (minimum) width (or height), preserving aspect ratio:
            '-resize', params[:width].to_s+"x"+params[:width].to_s+"^",
            # write to a jpg file with the given output filename:
            "jpg:"+output_filename 
        )
    end
end
