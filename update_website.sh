#!/usr/bin/bash

# Run this script in a cronjob on the server for automated local deployment of updates.
# Note: Make sure that all necessary PATH values are accessible to cron before running this!

# print what is executed, and stop if something fails
set -xEeuo pipefail

# go to the website source code directory (where this script is located)
cd $(realpath $(dirname "$0"))

# pull the latest version of the source code from codeberg
git pull

# tell bundle not to change the lock file
export BUNDLE_FROZEN=true

# compile a new version of the website
bundle install
bundle exec nanoc

# deploy it
bundle exec nanoc deploy