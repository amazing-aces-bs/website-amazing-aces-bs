// filter by a regular expression, case-insensitive
$.expr[":"].contains = $.expr.createPseudo(arg => elem => $(elem).text().toUpperCase().indexOf(arg.toUpperCase()) >= 0);

const filter = function(term, updateBox) {
    if (updateBox) {
        $("#filter-search").val(term);
    }

    if (term === "") {
        window.history.replaceState("", "", window.originalURL);
    } else {
        window.location.href = window.originalURL+"#"+term;
        window.history.replaceState("", "", window.originalURL+"#"+term);
    }

    if (term === "") {
        $("#content .box").show();
        $("#content .boxes").show();
    } else {
        $("#content .box").hide();

        const ors = term.split("|");
        for (let o of ors) {
            o = o.trim();
            const ands = o.split(" ");
            let boxes = $("#content .box");
            for (let a of ands) {
                a = a.trim();
                boxes = boxes.has(":contains("+a+")");
            }
            boxes.show();
        }

        $("#content .boxes").show();
        $("#content .boxes").not(":has(:visible)").hide();
    }
};

$(function() {
    window.originalURL = window.location.pathname;

    $(window).on("hashchange", function() {
        const term = window.location.hash.substring(1);
        const hash = decodeURIComponent(term);
        filter(hash, true);
    });

    $("#filter-search").keyup(e => filter($("#filter-search").val(), false));

    $(window).trigger("hashchange");
});
