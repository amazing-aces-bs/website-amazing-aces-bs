---
title: "Amazing Aces Stammtisch im Mai 2024"
subtitle: Unser monatliches Treffen für alle Menschen im asexuellen Spektrum
tags: Veranstaltung
published: 2023-11-10
news: false
nometa: true
eventdate: 2024-05-09T19:00
eventname: "Ace-Stammtisch Mai 2024"
eventtype: "Stammtisch"
eventlocation: "Inselwall-Park"
---

*Für aktuelle Updates schaut am besten kurz vorher nochmal hier rein, abonniert unseren Kalender, oder lasst euch per E-mail erinnern.*

## Unser monatliches Treffen für alle Menschen im asexuellen Spektrum

### Do, 09.05.2024, Treffpunkt 19:00 Uhr am Eingang vom [Jugendzentrum Mühle](http://www.jugendzentrum-muehle.de/)

Du bist asexuell, demisexuell, oder gray-ace?
Oder du bist dir noch nicht ganz sicher, aber möchtest dich einfach gerne mit anderen Menschen des asexuellen Spektrums treffen und gemeinsam über eure Erfahrungen mit einer Welt sprechen, in der Asexualität noch viel zu oft unsichtbar gemacht wird?
Dann komm vorbei und lasst uns eine große Community werden!

**Achtung: das Treffen findet heute draußen statt!**  
Wir treffen uns um 19 Uhr am Eingang vom [Jugendzentrum Mühle](http://www.jugendzentrum-muehle.de/) (An der Neustadtmühle 3, 38100 Braunschweig) und gehen dann zusammen in den benachbarten Park. Bringt gerne Picknickdecken mit!

(Lasst euch vom Begriff "Jugendzentrum" nicht verwirren - unser Treffen liegt außerhalb der Jugendzentrums-Öffnungszeiten, und der Raum ist für uns reserviert. Bei den Amazing Aces sind Menschen in jedem Alter willkommen!)

Essen und Getränke (bitte nur alkoholfrei) können wir uns gerne selbst mitbringen.

Zu Beginn machen wir üblicherweise eine kurze Vorstellungsrunde, und überlegen, ob wir bestimmte Themen haben, die uns gerade beschäftigen.
Ob ihr danach dann einfach nur plaudern wollt oder lieber eine moderierte Diskussionsrunde, können wir zusammen jedes Mal neu entscheiden.  

Wir freuen uns über alle Menschen, die sich darüber freuen, hier eine Community zu finden :)

**Ein Hinweis zum Infektionsschutz:** Wir leben immer noch in einer Pandemie, und wollen uns nicht gegenseitig anstecken - also bleibt bitte zuhause, falls ihr Erkältungs-Symptome habt.