---
title: "Amazing Aces Stammtisch im Dezember 2023"
subtitle: Unser monatliches Treffen für alle Menschen im asexuellen Spektrum
tags: Veranstaltung
published: 2023-09-29
updated: 2023-10-06
news: false
nometa: true
eventdate: 2023-12-14T19:00
eventname: "Ace-Stammtisch Dezember 2023"
eventtype: "Stammtisch"
eventlocation: "Weihnachtsmarkt & Jugendzentrum Mühle"
---

*Für aktuelle Updates schaut am besten kurz vorher nochmal hier rein, abonniert unseren Kalender, oder lasst euch per E-mail erinnern.*

## Unser monatliches Treffen für alle Menschen im asexuellen Spektrum

### Do, 14.12.2023 ab 19:00 Uhr auf dem Weihnachtsmarkt, ab 20:30 Uhr im Jugendzentrum Mühle

Du bist asexuell, demisexuell, oder gray-ace?
Oder du bist dir noch nicht ganz sicher, aber möchtest dich einfach gerne mit anderen Menschen des asexuellen Spektrums treffen und gemeinsam über eure Erfahrungen mit einer Welt sprechen, in der Asexualität noch viel zu oft unsichtbar gemacht wird?
Dann komm vorbei und lasst uns eine große Community werden!

Zu unserem Stammtisch in der Vorweihnachtszeit bummeln wir etwas über den Braunschweiger Weihnachtsmarkt - wir treffen uns dort um 19 Uhr vor dem westlichen Eingang zum Weihnachtsmarkt (bei der großen Statue neben dem Landesmuseum). Ca. um 20 Uhr machen wir uns auf den Weg zu unserem erprobten Raum im **[Jugendzentrum Mühle](http://www.jugendzentrum-muehle.de/) (An der Neustadtmühle 3, 38100 Braunschweig)**. Wer beim Weihnachtsmarkt nicht dabei sein möchte, kann um 20:30 Uhr direkt zur Mühle kommen.

(Lasst euch vom Begriff "Jugendzentrum" nicht verwirren - unser Treffen liegt außerhalb der Jugendzentrums-Öffnungszeiten, und der Raum ist für uns reserviert. Bei den Amazing Aces sind Menschen in jedem Alter willkommen!)

Essen und Getränke (bitte nur alkoholfrei) können wir uns dort hin gerne selbst mitbringen.

Zu Beginn des Stammtischs machen wir üblicherweise eine kurze Vorstellungsrunde, und überlegen, ob wir bestimmte Themen haben, die uns gerade beschäftigen.
Ob ihr danach dann einfach nur plaudern wollt oder lieber eine moderierte Diskussionsrunde, können wir zusammen jedes Mal neu entscheiden.  
Es gibt in der Mühle außerdem eine Sammlung von Brett- und Kartenspielen, mit denen wir den Stammtisch bei Bedarf spontan zu einem Spiele-Abend verwandeln können.

Wir freuen uns über alle Menschen, die sich darüber freuen, hier eine Community zu finden :)

**Ein Hinweis zum Infektionsschutz:** Wir leben immer noch in einer Pandemie, und der Winter ist Grippe- und Erkältungszeit. Wir wollen uns nicht gegenseitig anstecken - also bleibt bitte zuhause, falls ihr Erkältungs-Symptome habt. Wir freuen uns außerdem, wenn wir vor dem Treffen einen Corona-Schnelltest macht, und empfehlen, im Innenraum und in dichtem Gedränge eine FFP2-Maske zu tragen, so lange ihr nicht gerade am Essen oder Trinken seid. Denkt auch daran, euch warm genug anzuziehen, damit wir den Raum öfter mal lüften können.
