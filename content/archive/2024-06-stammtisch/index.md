---
title: "Amazing Aces Stammtisch im Juni 2024"
subtitle: Unser monatliches Treffen für alle Menschen im asexuellen Spektrum
tags: Veranstaltung
published: 2023-11-10
news: false
nometa: true
eventdate: 2024-06-13T19:00
eventname: "Ace-Stammtisch Juni 2024"
eventtype: "Stammtisch"
eventlocation: "Jugendzentrum Mühle"
---

*Für aktuelle Updates schaut am besten kurz vorher nochmal hier rein, abonniert unseren Kalender, oder lasst euch per E-mail erinnern.*

## Unser monatliches Treffen für alle Menschen im asexuellen Spektrum

### Do, 13.06.2024 ab 19:00 Uhr im [Jugendzentrum Mühle](http://www.jugendzentrum-muehle.de/)

Du bist asexuell, demisexuell, oder gray-ace?
Oder du bist dir noch nicht ganz sicher, aber möchtest dich einfach gerne mit anderen Menschen des asexuellen Spektrums treffen und gemeinsam über eure Erfahrungen mit einer Welt sprechen, in der Asexualität noch viel zu oft unsichtbar gemacht wird?
Dann komm vorbei und lasst uns eine große Community werden!

Das Treffen findet im Erdgeschoss (Café/Offene-Tür-Bereich) des **[Jugendzentrums Mühle](http://www.jugendzentrum-muehle.de/) (An der Neustadtmühle 3, 38100 Braunschweig)** statt.

(Lasst euch vom Begriff "Jugendzentrum" nicht verwirren - unser Treffen liegt außerhalb der Jugendzentrums-Öffnungszeiten, und der Raum ist für uns reserviert. Bei den Amazing Aces sind Menschen in jedem Alter willkommen!)

Essen und Getränke (bitte nur alkoholfrei) können wir uns gerne selbst mitbringen.

Zu Beginn machen wir üblicherweise eine kurze Vorstellungsrunde, und überlegen, ob wir bestimmte Themen haben, die uns gerade beschäftigen.
Ob ihr danach dann einfach nur plaudern wollt oder lieber eine moderierte Diskussionsrunde, können wir zusammen jedes Mal neu entscheiden.  
Es gibt in der Mühle außerdem eine Sammlung von Brett- und Kartenspielen, mit denen wir den Stammtisch bei Bedarf spontan zu einem Spiele-Abend verwandeln können.

Wir freuen uns über alle Menschen, die sich darüber freuen, hier eine Community zu finden :)

**Ein Hinweis zum Infektionsschutz:** Wir leben immer noch in einer Pandemie, und wollen uns nicht gegenseitig anstecken - also bleibt bitte zuhause, falls ihr Erkältungs-Symptome habt. Wir freuen uns außerdem, wenn wir vor dem Treffen einen Corona-Schnelltest macht, und empfehlen, im Innenraum eine FFP2-Maske zu tragen, so lange ihr nicht gerade am Essen oder Trinken seid. Denkt auch daran, euch warm genug anzuziehen, damit wir den Raum öfter mal lüften können.