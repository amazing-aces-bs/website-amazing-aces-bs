---
title: Häufig gestellte Fragen
subtitle: Amazing Aces Brauschweig
nometa: true
menu: Häufige Fragen
thumbnail: /assets/images/amazing-aces-logo.png
---
<a name="top"></a>

- [Wer seid ihr?](#werseidihr)
- [Warum gibt es die Amazing Aces?](#warum)
- [Wer organisiert das?](#orga)
- [Wieviele Leute seid ihr?](#wieviele)
- [Sind immer die gleichen Leute da?](#diegleichenleute)
- [Wo kann ich euch vorher online kennenlernen oder mich mit anderen Leuten absprechen, die auch dabei sein werden?](#socialmedia)
- [Was passiert bei so einem Stammtisch?](#waspassiert)
- [Redet ihr nur über Asexualität?](#thema)
- [Was macht ihr sonst noch, außer Reden?](#wassonst)
- [Ich bin nicht sicher, ob ich ins asexuelle Spektrum passe, darf ich trotzdem kommen?](#nichtsicher)
- [Ich bin nicht asexuell, aber möchte mich über das Ace-Spektrum informieren (z.B. weil mein\*e Partner\*in asexuell ist), darf ich trotzdem kommen?](#nichtace)
- [Wo findet der Stammtisch statt?](#wo)
- [Muss ich da etwas zu essen oder trinken kaufen?](#essen)
- [Ist der Zugang zum Stammtisch rollstuhlgerecht?](#rollstuhl)
- [Wann findet der Stammtisch statt?](#wann)

##<a name="werseidihr"></a>Wer seid ihr?

Wir sind eine Community für Menschen in Braunschweig und Umgebung, die sich im [asexuellen Spektrum](/das-asexuelle-spektrum/) identifizieren. Wer selbst im Ace-Spektrum ist (oder vermutet, vielleicht im Ace-Spektrum zu sein), kann zu unserem Stammtisch kommen. Und wer sich über das asexuelle Spektrum informieren möchte, findet dazu Infos auf unserer Website.

<p class="up" markdown ="1">[▲ nach oben](#top)</p>

##<a name="warum"></a>Warum gibt es die Amazing Aces?

Asexualität ist in unserer Kultur oft unsichtbar. Viele Asexuelle denken, sie seien die Einzigen, denen es so geht, und fürchten, dass etwas mit ihnen nicht in Ordnung sei. Erfahrungsaustausch mit anderen Asexuellen ist hilfreich, um zu erkennen, dass Asexualität häufiger ist als vermutet und genauso in Ordnung ist, wie alle anderen sexuellen Orientierungen auch.

Deshalb wollen wir einen Raum schaffen, in dem wir uns mit anderen Menschen des asexuellen Spektrums austauschen können, und haben dafür im Oktober 2017 einen regelmäßigen Stammtisch ins Leben gerufen.

<p class="up" markdown ="1">[▲ nach oben](#top)</p>

##<a name="orga"></a>Wer organisiert das?

Der Stammtisch wurde 2017 von **Kiki** und **Lena** gegründet, die seit 2025 nicht mehr in Brauschweig wohnen.  
Seit 2024 ist **Liya** mit im Orga-Team. Wir sorgen dafür, dass die Termine auf dieser Website und an anderen Orten angekündigt werden, und sind auch für euch [ansprechbar](/kontakt/), falls ihr Fragen habt.  

Wenn ihr Ideen habt, könnt ihr die auch gerne einbringen, wir bieten euch nur den Rahmen, der von euch mit Inhalten gefüllt werden kann. Wir freuen uns auch, wenn sich noch weitere Menschen an der Organisation [beteiligen](/orga-mitmachen/) mögen - sprecht uns gerne an!

**Liya:**  

> *Ich wurde 2002 geboren und bin nicht-binär, sowie auf dem asexuellen und aromatischem Spektrum.  Außerdem studiere ich Informatik und betreue in den Sommerferien in einem Zeltlager mit. Ich bin im August 2023 dem Stammtisch beigetreten und seit Mai 2024 Teil des Orga-Teams.*

**Kiki:**  

> *Ich wurde 1987 geboren und verstehe mich seit 2015 als irgendwo im asexuellen Spektrum (meist bezeichne ich mich als gray-ace). Außerdem lebe ich polyamor, arbeite als Software-Entwicklerin und bin selbständig als Schneidermeisterin.*  
> *Ich habe die Amazing Aces mitgegründet, weil mir die Sichtbarkeit des asexuellen Spektrums wichtig ist (hätte ich mehr darüber gewusst, wäre mein Coming-out sicherlich früher und einfacher gewesen). Dazu möchte ich eine Community schaffen, in der wir Aces uns nicht als Außenseiter\*innen fühlen müssen und uns mit unseren Erfahrungen gegenseitig unterstützen können.*  
>  
> *(Seit 2025 bin ich nicht mehr am Orga-Team beteiligt, da ich aus Braunschweig weggezogen bin.)*

**Lena:**  

> *Ich wurde 1984 geboren und bin lesbisch, trans, polyamor und im asexuellen Spektrum. Wo ich mich dort genau einordne, hat sich schon ein paar mal geändert und ist auch jetzt noch im Fluss. Ich bin Software-Entwicklerin und mache hier und da ein bisschen queeren Aktivismus im kleinen Stil.*  
> *Ich habe die Amazing Aces mitgegründet, weil ich in anderen Kontexten bereits erlebt habe, wie wertvoll es sein kann, sich mit Menschen in einer ähnlichen Lebenssituation auszutauschen.*  
>  
> *(Seit 2025 bin ich nicht mehr am Orga-Team beteiligt, da ich aus Braunschweig weggezogen bin.)*

<p class="up" markdown ="1">[▲ nach oben](#top)</p>

##<a name="wieviele"></a>Wieviele Leute seid ihr?

Beim Stammtisch sind wir bisher (Stand: Sommer 2024) oft eher eine kleine Gruppe, meistens zwischen drei und sieben Leute. Wir freuen uns immer über neue Gesichter :)

<p class="up" markdown ="1">[▲ nach oben](#top)</p>

##<a name="diegleichenleute"></a>Sind immer die gleichen Leute da?

Normalerweise ist immer mindestens eine von uns Organisator\*innen da - ansonsten gibt es ein paar Menschen, die oft da sind, aber nicht jedes Mal. Und einige, die eher selten da sind, oder auch nur einmal da waren. Wir freuen uns immer über neue Teilnehmer\*innen.

<p class="up" markdown ="1">[▲ nach oben](#top)</p>

##<a name="socialmedia"></a>Wo kann ich euch vorher online kennenlernen oder mich mit anderen Leuten absprechen, die auch dabei sein werden?

Da gibt es mehrere Möglichkeiten: 

- Wenn du eine konkrete Frage hast, kannst du uns Organisator\*innen auch eine [E-mail](/kontakt/) schreiben - wir können die Frage ggf. auch per E-mail an die Leute weiterleiten, die unsere Termin-Erinnerungen und Nachrichten abonniert haben.
- Wir haben Gruppen bei **Signal** und **Whatsapp**, in denen sich Teilnehmer\*innen und Interessierte online austauschen können. Wenn ihr in diese Gruppen rein möchtet (oder wenn ihr Interesse daran hättet, eine Gruppe auf einem anderen Messenger zu nutzen), [sagt uns Bescheid](/kontakt/)!

<p class="up" markdown ="1">[▲ nach oben](#top)</p>

##<a name="waspassiert"></a>Was passiert denn bei so einem Stammtisch?

Meistens sitzen wir gemütlich in einer Runde auf Sofas, Picknickdecken oder in der Videokonferenz und unterhalten uns, manchmal haben wir dabei auch etwas zu Essen und zu Trinken. 
Wenn neue Leute dabei sind, machen wir am Anfang meistens eine kurze Vorstellungsrunde.  
Wenn Jemand ein bestimmtes Thema vorschlägt (manchmal schicken wir auch Themenvorschläge vorher per e-mail rum), dann reden wir zuerst über dieses Thema - anonsten bzw. danach über irgendwas, was uns spontan so einfällt.

<p class="up" markdown ="1">[▲ nach oben](#top)</p>

##<a name="thema"></a>Redet ihr nur über Asexualität?

Häufig reden wir über Themen, die mit Asexualität zu tun haben. 
Z.B. erzählen wir von unseren Coming-out-Erfahrungen oder unterhalten uns über Bücher, in denen Asexualität vorkommt, etc.  
Und manchmal reden wir einfach über irgendwas ganz anderes, was uns gerade interessiert - um eine angenehme Community aufzubauen und uns besser kennenzulernen.  
Welche Gesprächsthemen es an einem bestimmten Abend gibt, hängt davon ab, was die anwesenden Leute gerade besprechen wollen. Wir sind immer offen für Vorschläge.

<p class="up" markdown ="1">[▲ nach oben](#top)</p>

##<a name="wassonst"></a>Was macht ihr sonst noch, außer Reden?

Manchmal bringt jemand etwas zu essen mit, z.B. einen Kuchen. Außerdem gibt es vor Ort eine große Sammlung an Brettspielen, von denen wir manchmal etwas spielen. An sich ist der Stammtisch aber schon hauptsächlich zum Reden gedacht. Natürlich dürft ihr auch beim Reden nebenbei etwas mit euren Händen machen, wie z.B. Stricken, Zeichnen, ...

Wir könnten uns auch vorstellen, andere Sachen zu machen, wie z.B. zusammen einen Film gucken, Demo-Schilder für den CSD basteln, oder was auch immer euch so einfällt - allerdings würden wir das vorher ankündigen und vielleicht auch einen extra-Termin dafür suchen, damit der eigentliche Stammtisch einfach eine Gesprächsrunde bleiben darf.

<p class="up" markdown ="1">[▲ nach oben](#top)</p>

##<a name="nichtsicher"></a>Ich bin mir nicht sicher, ob ich ins asexuelle Spektrum passe. Darf ich trotzdem dabei sein?

Ja, auch wenn du dir nicht sicher bist, bist du bei uns herzlich willkommen - vielleicht hilft der Austausch mit anderen Aces ja sogar dabei, etwas über dich selbst zu lernen.

<p class="up" markdown ="1">[▲ nach oben](#top)</p>

##<a name="nichtace"></a>Ich bin selbst nicht asexuell, aber ich möchte mich über Asexualität informieren, z.B. weil ich vermute, dass mein\*e Partner\*in asexuell ist. Darf ich auch dabei sein?

Das ist so allgemein schwierig zu beantworten. In erster Linie ist der Stammtisch ein Ort für uns Asexuelle (und Leute, die unsicher sind, ob sie asexuell sind) selbst, aber wir können uns auch sinnvolle Ausnahmen vorstellen. Am besten schreibst du uns eine E-mail und erklärst deine Situation, dann können wir überlegen ob es passt, wenn du beim Stammtisch dabei bist.

<p class="up" markdown ="1">[▲ nach oben](#top)</p>

##<a name="wo"></a>Wo findet das Treffen statt?

Aktuell (Stand: Herbst 2023) treffen wir uns im Erdgeschoss (Café/Offene-Tür-Bereich) des [Jugendzentrums Mühle](http://www.jugendzentrum-muehle.de/) (An der Neustadtmühle 3, 38100 Braunschweig).

Wenn das Wetter es zulässt, treffen wir uns auch gerne mal draußen an der frischen Luft, und machen zusammen ein Picknick.  
Und ab und zu gibt es ein Online-Treffen, d.h. in einem Videokonferenz-Raum mit BigBlueButton. 

<p class="up" markdown ="1">[▲ nach oben](#top)</p>

##<a name="essen"></a>Kann/muss ich da etwas zu essen oder trinken kaufen?

Zu kaufen gibt es nichts - aber Essen und Getränke dürft ihr gerne mitbringen, vor allem natürlich bei den Picknick-Treffen. In der Mühle gibt es auch eine Küche, wo wir uns z.B. Tee kochen können. 

<p class="up" markdown ="1">[▲ nach oben](#top)</p>

##<a name="rollstuhl"></a>Ist der Zugang zum Stammtisch rollstuhlgerecht?

Der Eingang zum Jugendzentrum Mühle hat im Außenbereich etwas holpriges Pflaster und eine Eingangstür mit einer kleinen Schwelle. Die Tür lässt sich mit einer Griffstange nach außen aufziehen, sie ist allerdings relativ schwer. Es gibt keinen automatischen Türöffner und keine Klingel - wir können aber vom Café aus sehen, ob jemand draußen vor der Tür ist, bzw. können hören, wenn jemand an die Tür klopft.

![Außenbereich vor dem Eingang, mit etwas holprigen Pflastersteinen, aber ohne große Stufen](jz_muehle_eingangsbereich_aussen.jpg)  

![Blick auf den Boden bei offener Eingangstür, so dass die Türschwelle zu sehen ist.](jz_muehle_eingangsbereich_tuerschwelle.jpg)  

Der Zugang zu den Toiletten ist vom Eingangsbereich aus stufenlos.  
![Blick von der Eingangstür durch den Flur im Erdgeschoss. Auf der rechten Seite ist unter anderem eine Toilettentür mit Rollstuhl-Symbol zu sehen. Geradeaus ist die Glastür zum Café. Hinter der Glastür ist auf dem Foto allerdings nichts zu sehen, weil das Licht aus ist.](jz_muehle_eingangsbereich_innen.jpg)

Vom Eingangsbereich und WC aus führen drei Stufen nach unten in den Café-Raum, wo unser Treffen stattfindet.
![Blick aus dem Café-Bereich in Richtung Flur. Der Boden des Cafés ist niedriger als der Flur, und der Übergang besteht aus drei Stufen (in üblicher Treppenstufen-Höhe), die über die ganze Breite des Flurs gerade sind. An der linken Seite, wo sie überstehen und ins Café hineinragen, sind sie abgerundet. An der Wand neben den Stufen gibt es kein Geländer.](jz_muehle_cafe_mit_treppe_innen_blickrichtung_zum_eingang.jpg)

Bei Picknick-Treffen können wir uns jederzeit einen Platz suchen, der gut zugänglich ist. Und Online-Treffen sind ja sowieso Orts-unabhängig.

<p class="up" markdown ="1">[▲ nach oben](#top)</p>

##<a name="wann"></a>Wann findet der Stammtisch statt?

Üblicherweise am zweiten Donnerstag des Monats um 19 Uhr. Die [aktuellen Termine](/termine/) findet ihr auch immer hier auf der Website, falls sich mal etwas ändert oder das Treffen z.B. wegen eines Feiertags ausfällt oder an einem anderen Ort stattfindet.  
Wenn du an die Termine erinnert werden möchtest, gib uns gerne deine E-mail-Adresse. Wir schicken immer ein paar Tage vor dem nächsten Termin eine Erinnerungsmail an alle, die das möchten.

Falls dir der Termin am Donnerstag Abend generell nicht passt, aber du eigentlich gerne dabei sein möchtest, dann freuen wir uns über eine [Rückmeldung](/kontakt/) dazu - falls viele Leute lieber einen anderen Termin hätten, können wir dann überlegen, wann ein besserer Zeitpunkt wäre.

<p class="up" markdown ="1">[▲ nach oben](#top)</p>
