---
title: "Macht mit im Orga-Team!"
subtitle: "Wir gestalten zusammen die Zukunft der Amazing Aces"
tags: Orga, Community
published: 2024-04-12
updated: 2024-12-09
news: true
---

Wir möchten alle einladen, die bei den Amazing Aces eine Community gefunden haben: Bringt euch ein und helft mit, die Zukunft dieser Gruppe zu gestalten.

Das Orga-Team waren zu Beginn "offiziell" nur Kiki und Lena. Seit Frühjahr 2024 ist Liya mit dabei. Liya übernimmt die Orga-Aufgaben ab 2025 erstmal alleine, nachdem Kiki und Lena aus Braunschweig weg ziehen.

Einige andere von euch haben auch bisher schon manchmal mitgeholfen, und das freut uns sehr. Damit die Amazing Aces auch dann weiter laufen, wenn einige von uns mal nicht da sind, würden wir das Orga-Team gerne auch offiziell vergrößern. Wer mithelfen mag: [meldet euch](/kontakt) gerne!

## Was macht denn so ein Orga-Team eigentlich?

Gute Frage!  

Da gibt es sehr verschiedene Bereiche. Hier eine Liste davon, was wir bisher manchmal zu tun haben:

**E-mail-Erinnerungen:**  

- Ein paar Tage vor jedem Stammtisch eine Erinnerungs-E-mail schicken
- Die Liste der Leute, an die solche Mails geschickt werden sollen, aktuell halten

**Stammtische:**  

- Nachmittags beim JZ Mühle den Schlüssel abholen
- Abends kurz vor Beginn da sein und die Tür aufschließen
- "Moderation" bzw. einleitende Worte beim Stammtisch 
- Am Ende die Türen zuschließen und den Schlüssel zurückgeben
- Bei Online-Stammtischen: den Videokonferenz-Raum öffnen und für technische Fragen da sein

**Planung:**  

- Alle paar Monate beim JZ Mühle anrufen oder eine E-mail schreiben, damit der Raum weiterhin zur passenden Zeit für uns reserviert ist
- Falls mal ein Termin verschoben werden soll, auch dafür die Raum-Belegung abklären
- Über den neuen Ort und Zeit entscheiden, wenn der Stammtisch (einmal oder dauerhaft) verlegt werden soll

**Website:**  

- Termin-Infos auf unserer Website aktualisieren
- Blog-Posts für unsere Website schreiben
- Dafür sorgen, dass unser Server und unsere Domain rechtzeitig bezahlt werden, damit unsere Website und unser e-mail-Postfach erreichbar bleiben

**Ansprechperson sein:**  

- E-mails beantworten, die an unsere Kontakt-Adresse geschickt wurden
- Moderation in unserer Signal- und/oder Whatsapp-Gruppe

**Sichtbarkeit:**  

- Texte schreiben oder korrigieren, die in anderen Medien über uns veröffentlicht werden
- Veranstaltungen wie z.B. den queeren runden Tisch der Stadt Braunschweig besuchen
- Besondere Veranstaltungen organisieren (z.B. Info-Stand beim CSD)
- Werbe-Flyer für uns entwerfen, drucken lassen und auslegen

Das klingt vielleicht erstmal nach viel, aber die meisten Aufgaben brauchen nur ein paar Minuten im Monat. Und natürlich muss auch nicht jede\*r immer alles davon machen - je mehr Leute wir sind, desto entspannter wird es :) Und nichts davon ist in Stein gemeißelt, denn das Orga-Team kann natürlich in Zukunft gemeinsam Änderungen beschließen, wenn z.B. ein Kommunikationsmedium dazu kommen oder wegfallen soll.

Wir freuen uns auf Zuwachs im Orga-Team - [meldet euch](/kontakt), wenn ihr auch dabei sein wollt!